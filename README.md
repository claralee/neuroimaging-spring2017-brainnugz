# README #

This is the README for the scripting done for the Brain Nugz final Neuroimaging project.

### Analysis and dependencies ###

* SPM documentation
* MATLAB 
* The bulk of this code was taken from the SPM batch editor .m code (View > .m code) for each module (preprocessing stages, 1st level analyses, 2nd level analyses)
* Split into: 1) Preprocessing and Single Subject, 2) Group level for SuccessfulStop, 3) Group level for UnsuccessfulStop, 4) Group level for SS>US Difference

Code from each SPM module was combined and put into a loop that ran through each subject number, pulling each subject's individual data and performing preprocessing/SSM/Group level analysis on that subject. Each step was stored in a matlabbatch{#}, with the # depicting the stage order. When spm_jobman was called, it ran through all the stages stored inside the matlabbatch. Options were able to be edited into the script for each module. 

### Who do I talk to? ###

* Brain Nugz group