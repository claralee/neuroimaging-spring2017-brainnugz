%% Initiation

% We use the SPM jobman function to initiate our preprocessing scripting.
% We set the path directory and SPM directories, and define what subjects
% the script will be run on. 
spm('defaults','fmri');
spm_jobman('initcfg');

pathDir = 'C:\Users\strik\Documents\MATLAB\ds000030\';
spmDir = 'C:\Users\strik\Documents\MATLAB\spm12\';

subList = {'sub-10159';'sub-10171';'sub-10189';'sub-10206';'sub-10217';'sub-10225'; ...
    'sub-10227';'sub-10228';'sub-10235';'sub-10249';'sub-10269';'sub-10271';'sub-10273'; ...
    'sub-10274';'sub-10280';'sub-10290';'sub-10292';'sub-10304';'sub-10316';'sub-10321'};

% Here, we are looping through all of the subjects listed above and
% dictating that if a directory for outputs does not exist yet, it will be
% created. The Nifti file is extracted, the number of slices is read, and
% the overall directory for the subject is set. 

for subID = 1:size(subList)
    clear matlabbatch
    
    %create the outward directory where all created files go and are saved
    outDir = [pathDir subList{subID} '\func\output'];
    mkdir(outDir);
    cd(outDir);
    
    %read in the Nifti object from each subject's data and extract the
    %total number of slices/volumes to be used in later preprocessing steps
    niftiObject= nifti([pathDir subList{subID} '\func\' subList{subID} '_task-stopsignal_bold.nii']);
    niftiData = niftiObject.dat;
    nSlices = size(niftiData,4);
    
    %get path of the stopsignal data file for each subject
    subDataPath = [pathDir subList{subID} '\func\' subList{subID} '_task-stopsignal_bold.nii'];
    
    %We are using a nested cell structure to store this data because that
    %was how the batch was originally coded in the SPM .m file, retrieved
    %from the batch editor
    %Initilize empty 1x1 outer cell and inner contained cell sized to number of slices
    dataRealign = cell(1,1);
    innerDataRealign = cell (nSlices,1);
    
    %The inner cell will contain the path to the data and the slice number, in string format
    for sliceNum = 1:nSlices
        slicePath = [subDataPath ',' sprintf('%d',sliceNum)];
        innerDataRealign(sliceNum, 1) = {slicePath};
    end
    
    %store inner cell data inside the outer cell
    dataRealign(1,1) = {innerDataRealign};    
%% Realignment

% 1. Parameters are set for realignment. Matlabbatch is notated in the order
% that the script should run.
    matlabbatch{1}.spm.spatial.realign.estwrite.data = dataRealign;
    
    % Estimate the images.
    matlabbatch{1}.spm.spatial.realign.estwrite.eoptions.quality = 0.9;
    matlabbatch{1}.spm.spatial.realign.estwrite.eoptions.sep = 4;
    matlabbatch{1}.spm.spatial.realign.estwrite.eoptions.fwhm = 5;
    matlabbatch{1}.spm.spatial.realign.estwrite.eoptions.rtm = 1;
    matlabbatch{1}.spm.spatial.realign.estwrite.eoptions.interp = 2;
    matlabbatch{1}.spm.spatial.realign.estwrite.eoptions.wrap = [0 0 0];
    matlabbatch{1}.spm.spatial.realign.estwrite.eoptions.weight = '';
    
    % Write the files.
    matlabbatch{1}.spm.spatial.realign.estwrite.roptions.which = [0 1];
    matlabbatch{1}.spm.spatial.realign.estwrite.roptions.interp = 4; %interpolation: 4th degree B-spline
    matlabbatch{1}.spm.spatial.realign.estwrite.roptions.wrap = [0 0 0];
    matlabbatch{1}.spm.spatial.realign.estwrite.roptions.mask = 1;
    matlabbatch{1}.spm.spatial.realign.estwrite.roptions.prefix = 'r';
    realignmentFigure = spm_figure('FindWin','Graphics');
    print(realignmentFigure,'-djpeg','-r150','Realignment image.jpg')

%% Coregistration
% 2. We use the mean functional image as the reference, and the T1 as the
% source. 
    matlabbatch{2}.spm.spatial.coreg.estimate.ref = {[pathDir subList{subID} '\func\mean' subList{subID} '_task-stopsignal_bold.nii,1']};
    matlabbatch{2}.spm.spatial.coreg.estimate.source = {[pathDir subList{subID} '\anat\' subList{subID} '_T1w.nii,1']};
    matlabbatch{2}.spm.spatial.coreg.estimate.other = {''};
    matlabbatch{2}.spm.spatial.coreg.estimate.eoptions.cost_fun = 'nmi';
    matlabbatch{2}.spm.spatial.coreg.estimate.eoptions.sep = [4 2];
    matlabbatch{2}.spm.spatial.coreg.estimate.eoptions.tol = [0.02 0.02 0.02 0.001 0.001 0.001 0.01 0.01 0.01 0.001 0.001 0.001];
    matlabbatch{2}.spm.spatial.coreg.estimate.eoptions.fwhm = [7 7];
    coregistrationFigure = spm_figure('FindWin','Graphics');
    print(coregistrationFigure,'-djpeg','-r150','Coreg image.jpg')
   

%% Segmentation

% 3) T1 image is segmented into six tissue classifications that were
% previously denoted in SPM batch editor.
    matlabbatch{3}.spm.spatial.preproc.channel.vols = {[pathDir subList{subID} '\anat\' subList{subID} '_T1w.nii,1']};
    matlabbatch{3}.spm.spatial.preproc.channel.biasreg = 0.001;
    matlabbatch{3}.spm.spatial.preproc.channel.biasfwhm = 60;
    matlabbatch{3}.spm.spatial.preproc.channel.write = [0 0];
        
    % Tissue 1 - Grey matter
    matlabbatch{3}.spm.spatial.preproc.tissue(1).tpm = {[spmDir 'tpm\TPM.nii,1']};
    matlabbatch{3}.spm.spatial.preproc.tissue(1).ngaus = 1;
    matlabbatch{3}.spm.spatial.preproc.tissue(1).native = [1 0];
    matlabbatch{3}.spm.spatial.preproc.tissue(1).warped = [0 0];
      
    % Tissue 2 - White matter
    matlabbatch{3}.spm.spatial.preproc.tissue(2).tpm = {[spmDir 'tpm\TPM.nii,2']};
    matlabbatch{3}.spm.spatial.preproc.tissue(2).ngaus = 1;
    matlabbatch{3}.spm.spatial.preproc.tissue(2).native = [1 0];
    matlabbatch{3}.spm.spatial.preproc.tissue(2).warped = [0 0];
        
    % Tissue 3 - CSF
    matlabbatch{3}.spm.spatial.preproc.tissue(3).tpm = {[spmDir 'tpm\TPM.nii,3']};
    matlabbatch{3}.spm.spatial.preproc.tissue(3).ngaus = 2;
    matlabbatch{3}.spm.spatial.preproc.tissue(3).native = [1 0];
    matlabbatch{3}.spm.spatial.preproc.tissue(3).warped = [0 0];
    
    % Tissue 4 - Bone
    matlabbatch{3}.spm.spatial.preproc.tissue(4).tpm = {[spmDir 'tpm\TPM.nii,4']};
    matlabbatch{3}.spm.spatial.preproc.tissue(4).ngaus = 3;
    matlabbatch{3}.spm.spatial.preproc.tissue(4).native = [1 0];
    matlabbatch{3}.spm.spatial.preproc.tissue(4).warped = [0 0];
        
    % Tissue 5 - Soft tissue
    matlabbatch{3}.spm.spatial.preproc.tissue(5).tpm = {[spmDir 'tpm\TPM.nii,5']};
    matlabbatch{3}.spm.spatial.preproc.tissue(5).ngaus = 4;
    matlabbatch{3}.spm.spatial.preproc.tissue(5).native = [1 0];
    matlabbatch{3}.spm.spatial.preproc.tissue(5).warped = [0 0];
    
    % Tissue 6 - Air/background 
    matlabbatch{3}.spm.spatial.preproc.tissue(6).tpm = {[spmDir 'tpm\TPM.nii,6']};
    matlabbatch{3}.spm.spatial.preproc.tissue(6).ngaus = 2;
    matlabbatch{3}.spm.spatial.preproc.tissue(6).native = [0 0];
    matlabbatch{3}.spm.spatial.preproc.tissue(6).warped = [0 0];
    
    % Default batch editor settings.
    matlabbatch{3}.spm.spatial.preproc.warp.mrf = 1;
    matlabbatch{3}.spm.spatial.preproc.warp.cleanup = 1;
    matlabbatch{3}.spm.spatial.preproc.warp.reg = [0 0.001 0.5 0.05 0.2];
    matlabbatch{3}.spm.spatial.preproc.warp.affreg = 'mni';
    matlabbatch{3}.spm.spatial.preproc.warp.fwhm = 0;
    matlabbatch{3}.spm.spatial.preproc.warp.samp = 3;
    matlabbatch{3}.spm.spatial.preproc.warp.write = [0 1];

    %% Normalize
    
    %Initializes a cell structure sized to number of slices + 1 (to account
    %for the structural T1 image.
    %This time we are not using a nested cell structure because that is
    %how the original batch SPM .m file was coded. 
    innerDataNorm = cell(nSlices+1,1);
    getT1Path = [pathDir subList{subID} '\anat\' subList{subID} '_T1w.nii,1'];
    innerDataNorm(1,1) = {getT1Path};
    
    for sliceNum = 2:size(niftiData,4)+1
   
        slicePath = [subDataPath ',' sprintf('%d',sliceNum-1)];
    
        %inserts paths with slice number into the inner nested cell
        innerDataNorm(sliceNum, 1) = {slicePath};
    
    end
    
   % The y file (previously estimated warping vectors) is pulled to run normalization    
    matlabbatch{4}.spm.spatial.normalise.write.subj.def = {[pathDir subList{subID} '\anat\y_' subList{subID} '_T1w.nii']};
    matlabbatch{4}.spm.spatial.normalise.write.subj.resample = innerDataNorm;
    matlabbatch{4}.spm.spatial.normalise.write.woptions.bb = [-78 -112 -70
                                                              78 76 85];
    matlabbatch{4}.spm.spatial.normalise.write.woptions.vox = [2 2 2]; %voxel sizes
    matlabbatch{4}.spm.spatial.normalise.write.woptions.interp = 4; %interpolation
    matlabbatch{4}.spm.spatial.normalise.write.woptions.prefix = 'w';
    normalizeFigure = spm_figure('FindWin','Graphics');
    
    %saves and prints figures of normalized image
    print(normalizeFigure,'-djpeg','-r150','Normalised image.jpg')
   
    %% Smoothing
    innerDataSmooth = cell(nSlices,1);
    
    for sliceNum = 1:nSlices
    
        slicePath = [pathDir subList{subID} '\func\w' subList{subID} '_task-stopsignal_bold.nii,' sprintf('%d',sliceNum)];
    
        %inserts paths with slice number into the inner nested cell
        innerDataSmooth(sliceNum, 1) = {slicePath};
    
    end
   
    matlabbatch{5}.spm.spatial.smooth.data = innerDataSmooth;
    matlabbatch{5}.spm.spatial.smooth.fwhm = [8 8 8];% FWHM - [8 8 8] smoothing kernel  x, y, z 
    matlabbatch{5}.spm.spatial.smooth.dtype = 0;
    matlabbatch{5}.spm.spatial.smooth.im = 0;
    matlabbatch{5}.spm.spatial.smooth.prefix = 's';
    smoothFigure = spm_figure('FindWin','Graphics');
    print(smoothFigure,'-djpeg','-r150','Smoothed image.jpg')    
    
    %% Single Subject Modeling

    matlabbatch{6}.spm.stats.fmri_spec.dir = {[pathDir 'SSM\' subList{subID}]};
    matlabbatch{6}.spm.stats.fmri_spec.timing.units = 'secs';
    matlabbatch{6}.spm.stats.fmri_spec.timing.RT = 2;
    matlabbatch{6}.spm.stats.fmri_spec.timing.fmri_t = 16;
    matlabbatch{6}.spm.stats.fmri_spec.timing.fmri_t0 = 8;
    %%
    innerDataSSM = cell(nSlices,1);
    for sliceNum = 1:nSlices  
        slicePath = [pathDir subList{subID} '\func\sw' subList{subID} '_task-stopsignal_bold.nii,' sprintf('%d',sliceNum)];   
        %inserts paths with slice number into the inner nested cell
        innerDataSSM(sliceNum, 1) = {slicePath};   
    end
    %%
    %read in the onset times for Successful Stop and Unsuccessful Stop
    %conditions to create the contrasts 
    SucStopOnsetTimes = dlmread([pathDir subList{subID} '\func\' subList{subID} '_SuccessfulStop_onset_times.tsv'],' ',1,0);
    UnsucStopOnsetTimes = dlmread([pathDir subList{subID} '\func\' subList{subID} '_UnsuccessfulStop_onset_times.tsv'],' ',1,0);

    matlabbatch{6}.spm.stats.fmri_spec.sess.scans = innerDataSSM;
    matlabbatch{6}.spm.stats.fmri_spec.sess.cond(1).name = 'SuccessfulStop'; % condition 1
    matlabbatch{6}.spm.stats.fmri_spec.sess.cond(1).onset = SucStopOnsetTimes; %onset times
    matlabbatch{6}.spm.stats.fmri_spec.sess.cond(1).duration = 1.5; %duration time of task trials
    matlabbatch{6}.spm.stats.fmri_spec.sess.cond(1).tmod = 0;
    matlabbatch{6}.spm.stats.fmri_spec.sess.cond(1).pmod = struct('name', {}, 'param', {}, 'poly', {});
    matlabbatch{6}.spm.stats.fmri_spec.sess.cond(1).orth = 1;
    matlabbatch{6}.spm.stats.fmri_spec.sess.cond(2).name = 'UnsuccessfulStop'; % condition 2
    matlabbatch{6}.spm.stats.fmri_spec.sess.cond(2).onset = UnsucStopOnsetTimes; %onset times
    matlabbatch{6}.spm.stats.fmri_spec.sess.cond(2).duration = 1.5;
    matlabbatch{6}.spm.stats.fmri_spec.sess.cond(2).tmod = 0;
    matlabbatch{6}.spm.stats.fmri_spec.sess.cond(2).pmod = struct('name', {}, 'param', {}, 'poly', {});
    matlabbatch{6}.spm.stats.fmri_spec.sess.cond(2).orth = 1;
    matlabbatch{6}.spm.stats.fmri_spec.sess.multi = {''};
    matlabbatch{6}.spm.stats.fmri_spec.sess.regress = struct('name', {}, 'val', {});
    matlabbatch{6}.spm.stats.fmri_spec.sess.multi_reg = {[pathDir subList{subID} '\func\rp_' subList{subID} '_task-stopsignal_bold.txt']}; %rp file for motion corrected data
    matlabbatch{6}.spm.stats.fmri_spec.sess.hpf = 128;
    matlabbatch{6}.spm.stats.fmri_spec.fact = struct('name', {}, 'levels', {});
    matlabbatch{6}.spm.stats.fmri_spec.bases.hrf.derivs = [0 0];
    matlabbatch{6}.spm.stats.fmri_spec.volt = 1;
    matlabbatch{6}.spm.stats.fmri_spec.global = 'None';
    matlabbatch{6}.spm.stats.fmri_spec.mthresh = 0.8;
    matlabbatch{6}.spm.stats.fmri_spec.mask = {''};
    matlabbatch{6}.spm.stats.fmri_spec.cvi = 'AR(1)';

    %% Estimate

    matlabbatch{7}.spm.stats.fmri_est.spmmat = {[pathDir 'SSM\' subList{subID} '\SPM.mat']};
    matlabbatch{7}.spm.stats.fmri_est.write_residuals = 0;
    matlabbatch{7}.spm.stats.fmri_est.method.Classical = 1;

    
    %% Contrasts
    
    matlabbatch{8}.spm.stats.con.spmmat = {[pathDir 'SSM\' subList{subID} '\SPM.mat']};
    matlabbatch{8}.spm.stats.con.consess{1}.tcon.name = 'Successful Stop'; %condition 1
    matlabbatch{8}.spm.stats.con.consess{1}.tcon.convec = [1 0]; %contrast 1
    matlabbatch{8}.spm.stats.con.consess{1}.tcon.sessrep = 'none';
    matlabbatch{8}.spm.stats.con.consess{2}.tcon.name = 'Unsuccessful Stop'; %condition 2
    matlabbatch{8}.spm.stats.con.consess{2}.tcon.convec = [0 1];%contrast 2
    matlabbatch{8}.spm.stats.con.consess{2}.tcon.sessrep = 'none';
    matlabbatch{8}.spm.stats.con.consess{3}.tcon.name = 'Successful > Unsuccessful'; %condition 3 - difference in activation
    matlabbatch{8}.spm.stats.con.consess{3}.tcon.convec = [1 -1];%contrast 3 (difference)
    matlabbatch{8}.spm.stats.con.consess{3}.tcon.sessrep = 'none';
    matlabbatch{8}.spm.stats.con.delete = 1;
    
    %% Results reporting
    matlabbatch{9}.spm.stats.results.spmmat = {[pathDir 'SSM\' subList{subID} '\SPM.mat']};
    matlabbatch{9}.spm.stats.results.conspec.titlestr = '';
    matlabbatch{9}.spm.stats.results.conspec.contrasts = 1;
    matlabbatch{9}.spm.stats.results.conspec.threshdesc = 'FWE'; %multiple comparisons correction
    matlabbatch{9}.spm.stats.results.conspec.thresh = 0.05; %threshold value p < .05
    matlabbatch{9}.spm.stats.results.conspec.extent = 0;
    matlabbatch{9}.spm.stats.results.conspec.conjunction = 1;
    matlabbatch{9}.spm.stats.results.conspec.mask.none = 1;
    matlabbatch{9}.spm.stats.results.units = 1;
    matlabbatch{9}.spm.stats.results.export{1}.ps = true;
    matlabbatch{9}.spm.stats.results.export{2}.jpg = true;
    
    matlabbatch{10}.spm.stats.results.spmmat = {[pathDir 'SSM\' subList{subID} '\SPM.mat']};
    matlabbatch{10}.spm.stats.results.conspec.titlestr = '';
    matlabbatch{10}.spm.stats.results.conspec.contrasts = 2;
    matlabbatch{10}.spm.stats.results.conspec.threshdesc = 'FWE';
    matlabbatch{10}.spm.stats.results.conspec.thresh = 0.05;
    matlabbatch{10}.spm.stats.results.conspec.extent = 0;
    matlabbatch{10}.spm.stats.results.conspec.conjunction = 1;
    matlabbatch{10}.spm.stats.results.conspec.mask.none = 1;
    matlabbatch{10}.spm.stats.results.units = 1;
    matlabbatch{10}.spm.stats.results.export{1}.ps = true;
    matlabbatch{10}.spm.stats.results.export{2}.jpg = true;
    
    matlabbatch{11}.spm.stats.results.spmmat = {[pathDir 'SSM\' subList{subID} '\SPM.mat']};
    matlabbatch{11}.spm.stats.results.conspec.titlestr = '';
    matlabbatch{11}.spm.stats.results.conspec.contrasts = 3;
    matlabbatch{11}.spm.stats.results.conspec.threshdesc = 'FWE';
    matlabbatch{11}.spm.stats.results.conspec.thresh = 0.05;
    matlabbatch{11}.spm.stats.results.conspec.extent = 0;
    matlabbatch{11}.spm.stats.results.conspec.conjunction = 1;
    matlabbatch{11}.spm.stats.results.conspec.mask.none = 1;
    matlabbatch{11}.spm.stats.results.units = 1;
    matlabbatch{11}.spm.stats.results.export{1}.ps = true;
    matlabbatch{11}.spm.stats.results.export{2}.jpg = true;
    
    matlabbatch{12}.spm.stats.results.spmmat = {[pathDir 'SSM\' subList{subID} '\SPM.mat']};
    matlabbatch{12}.spm.stats.results.conspec.titlestr = '';
    matlabbatch{12}.spm.stats.results.conspec.contrasts = 1;
    matlabbatch{12}.spm.stats.results.conspec.threshdesc = 'none'; %uncorrected
    matlabbatch{12}.spm.stats.results.conspec.thresh = 0.005; %threshold p<.005
    matlabbatch{12}.spm.stats.results.conspec.extent = 0;
    matlabbatch{12}.spm.stats.results.conspec.conjunction = 1;
    matlabbatch{12}.spm.stats.results.conspec.mask.none = 1;
    matlabbatch{12}.spm.stats.results.units = 1;
    matlabbatch{12}.spm.stats.results.export{1}.ps = true;
    matlabbatch{12}.spm.stats.results.export{2}.jpg = true;
    
    matlabbatch{13}.spm.stats.results.spmmat = {[pathDir 'SSM\' subList{subID} '\SPM.mat']};
    matlabbatch{13}.spm.stats.results.conspec.titlestr = '';
    matlabbatch{13}.spm.stats.results.conspec.contrasts = 2;
    matlabbatch{13}.spm.stats.results.conspec.threshdesc = 'none';
    matlabbatch{13}.spm.stats.results.conspec.thresh = 0.005;
    matlabbatch{13}.spm.stats.results.conspec.extent = 0;
    matlabbatch{13}.spm.stats.results.conspec.conjunction = 1;
    matlabbatch{13}.spm.stats.results.conspec.mask.none = 1;
    matlabbatch{13}.spm.stats.results.units = 1;
    matlabbatch{13}.spm.stats.results.export{1}.ps = true;
    matlabbatch{13}.spm.stats.results.export{2}.jpg = true;
    
    
    matlabbatch{14}.spm.stats.results.spmmat = {[pathDir 'SSM\' subList{subID} '\SPM.mat']};
    matlabbatch{14}.spm.stats.results.conspec.titlestr = '';
    matlabbatch{14}.spm.stats.results.conspec.contrasts = 3;
    matlabbatch{14}.spm.stats.results.conspec.threshdesc = 'none';
    matlabbatch{14}.spm.stats.results.conspec.thresh = 0.005;
    matlabbatch{14}.spm.stats.results.conspec.extent = 0;
    matlabbatch{14}.spm.stats.results.conspec.conjunction = 1;
    matlabbatch{14}.spm.stats.results.conspec.mask.none = 1;
    matlabbatch{14}.spm.stats.results.units = 1;
    matlabbatch{14}.spm.stats.results.export{1}.ps = true;
    matlabbatch{14}.spm.stats.results.export{2}.jpg = true;
    
    
    spm_jobman('run',matlabbatch);
end