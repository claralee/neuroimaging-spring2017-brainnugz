%% Group Level Analysis for SuccessfulStop > UnsuccessfulStop difference condition

%% Initiation
spm('defaults','fmri');
spm_jobman('initcfg');
clear matlabbatch
pathDir = 'C:\Users\strik\Documents\MATLAB\ds000030\';
subCat = 'Controls';
conType = 'Diff_S_U';

subList = {'sub-10159';'sub-10171';'sub-10189';'sub-10206';'sub-10217';'sub-10225'; ...
    'sub-10227';'sub-10228';'sub-10235';'sub-10249';'sub-10269';'sub-10271';'sub-10273'; ...
    'sub-10274';'sub-10280';'sub-10290';'sub-10292';'sub-10304';'sub-10316';'sub-10321'};
%% GLM Design 
dataConFiles = cell (length(subList),1);

%getting con files from each subject's 1st level analysis, SS>US condition
for subID = 1:length(subList)
    conFiles = [pathDir 'SSM\' subList{subID} '\con_0003.nii,1'];
    dataConFiles(subID, 1) = {conFiles};
end 
matlabbatch{1}.spm.stats.factorial_design.dir = {[pathDir 'GLM\' subCat '\' conType]};
matlabbatch{1}.spm.stats.factorial_design.des.t1.scans = dataConFiles ; %design t1 = one sample t test 
matlabbatch{1}.spm.stats.factorial_design.cov = struct('c', {}, 'cname', {}, 'iCFI', {}, 'iCC', {});
matlabbatch{1}.spm.stats.factorial_design.multi_cov = struct('files', {}, 'iCFI', {}, 'iCC', {});
matlabbatch{1}.spm.stats.factorial_design.masking.tm.tm_none = 1;
matlabbatch{1}.spm.stats.factorial_design.masking.im = 1;
matlabbatch{1}.spm.stats.factorial_design.masking.em = {''};
matlabbatch{1}.spm.stats.factorial_design.globalm.gmsca.gmsca_no = 1;
matlabbatch{1}.spm.stats.factorial_design.globalm.glonorm = 1;

%% Estimate
matlabbatch{2}.spm.stats.fmri_est.spmmat = {[pathDir 'GLM\' subCat '\' conType '\SPM.mat']};
matlabbatch{2}.spm.stats.fmri_est.write_residuals = 0;
matlabbatch{2}.spm.stats.fmri_est.method.Classical = 1;

%% Contrasts
matlabbatch{3}.spm.stats.con.spmmat = {[pathDir 'GLM\' subCat '\' conType '\SPM.mat']};
matlabbatch{3}.spm.stats.con.consess{1}.tcon.name = 'Successful Stop'; %contrast condition
matlabbatch{3}.spm.stats.con.consess{1}.tcon.convec = [1];
matlabbatch{3}.spm.stats.con.consess{1}.tcon.sessrep = 'none';
matlabbatch{3}.spm.stats.con.delete = 1;



%% Results reporting
matlabbatch{4}.spm.stats.results.spmmat = {[pathDir 'GLM\' subCat '\' conType '\SPM.mat']};
matlabbatch{4}.spm.stats.results.conspec.titlestr = '';
matlabbatch{4}.spm.stats.results.conspec.contrasts = 1;
matlabbatch{4}.spm.stats.results.conspec.threshdesc = 'FWE'; %multiple comparisons correction
matlabbatch{4}.spm.stats.results.conspec.thresh = 0.05; %threshold p<.05
matlabbatch{4}.spm.stats.results.conspec.extent = 0;
matlabbatch{4}.spm.stats.results.conspec.conjunction = 1;
matlabbatch{4}.spm.stats.results.conspec.mask.none = 1;
matlabbatch{4}.spm.stats.results.units = 1;
matlabbatch{4}.spm.stats.results.export{1}.ps = true;
matlabbatch{4}.spm.stats.results.export{2}.jpg = true;

matlabbatch{5}.spm.stats.results.spmmat = {[pathDir 'GLM\' subCat '\' conType '\SPM.mat']};
matlabbatch{5}.spm.stats.results.conspec.titlestr = '';
matlabbatch{5}.spm.stats.results.conspec.contrasts = 1;
matlabbatch{5}.spm.stats.results.conspec.threshdesc = 'none'; %uncorrected
matlabbatch{5}.spm.stats.results.conspec.thresh = 0.005; %threshold p<.005
matlabbatch{5}.spm.stats.results.conspec.extent = 0;
matlabbatch{5}.spm.stats.results.conspec.conjunction = 1;
matlabbatch{5}.spm.stats.results.conspec.mask.none = 1;
matlabbatch{5}.spm.stats.results.units = 1;
matlabbatch{5}.spm.stats.results.export{1}.ps = true;
matlabbatch{5}.spm.stats.results.export{2}.jpg = true;

spm_jobman('run',matlabbatch)
